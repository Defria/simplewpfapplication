﻿using System;
using System.Windows.Input;

namespace SimpleWPFApplication.Commands
{
    public class RelayCommand : ICommand
    {
        private readonly Action _commandTask;

        public RelayCommand(Action doWork)
        {
            _commandTask = doWork;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            _commandTask();
        }
    }
}