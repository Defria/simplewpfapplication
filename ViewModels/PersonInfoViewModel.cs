﻿using System.Windows.Input;
using SimpleWPFApplication.Commands;
using SimpleWPFApplication.Models;

namespace SimpleWPFApplication.ViewModels
{
    public class PersonInfoViewModel : ViewModelBase
    {
        private string _name;
        private string _yourName;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public string YourName
        {
            get => _yourName;
            set
            {
                _yourName = value;
                OnPropertyChanged(nameof(YourName));
            }
        }

        public ICommand OnShowName => new RelayCommand(ShowName);

        private void ShowName()
        {
            var person = new Person()
            {
                Name = Name
            };
            YourName = $"Hello {person.Name}.";
        }
    }
}